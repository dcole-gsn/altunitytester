# AltUnityTester

AltUnity Tester is an open-source UI driven test automation tool that helps you find objects in your game and interacts with them using tests written in C#, Python or Java.

Join our Google Group for questions and discussions: https://groups.google.com/a/altom.com/forum/#!forum/altunityforum

Join our Gitter room here to chat with others in the community: https://gitter.im/AltUnityTester

Read the documentation on https://altom.gitlab.io/altunity/altunitytester 
 
