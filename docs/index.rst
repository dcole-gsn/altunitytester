.. AltUnity Tester documentation master file, created by
   sphinx-quickstart on Thu Oct 17 09:19:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AltUnity Tester's documentation!
=============================================

AltUnity Tester is an open-source UI driven test automation tool that helps you find objects in your game and interacts with them using tests written in C#, Python or Java. You can run your tests on real devices (mobile, PCs, etc.) or inside the Unity Editor.

Join our Google Group `here <https://groups.google.com/a/altom.com/forum/#!forum/altunityforum>`__ for questions and discussions.

Join our Gitter chat room `here <https://gitter.im/AltUnityTester>`__ to chat with us or with other members of the community.

.. toctree::
   :caption: Table of contents:
   :maxdepth: 3
   
   pages/overview
   pages/get-started
   pages/altunity-tester-gui
   pages/tester-with-cloud
   pages/examples   
   pages/commands      
   pages/advanced-usage
   pages/license
   pages/contributing

.. toctree::
    :caption: Community:

    Google Group <https://groups.google.com/a/altom.com/forum/#!forum/altunityforum>
    Gitter <https://gitter.im/AltUnityTester>
