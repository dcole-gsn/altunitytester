static void BuildFromCommandLine () {
    try {

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions ();
        buildPlayerOptions.scenes = new string[] {
            "Assets/AltUnityTesterExamples/Scenes/Scene 1 AltUnityDriverTestScene.unity",
            "Assets/AltUnityTesterExamples/Scenes/Scene 2 Draggable Panel.unity",
            "Assets/AltUnityTesterExamples/Scenes/Scene 3 Drag And Drop.unity",
            "Assets/AltUnityTesterExamples/Scenes/Scene 4 No Cameras.unity",
            "Assets/AltUnityTesterExamples/Scenes/Scene 5 Keyboard Input.unity",
            "Assets/AltUnityTesterExamples/Scenes/Scene6.unity"
        };

        buildPlayerOptions.locationPathName = "sampleGame.apk";
        buildPlayerOptions.target = BuildTarget.Android;
        // It's important to set options to Develoment otherwise AltUnity doesn't work
        buildPlayerOptions.options = BuildOptions.Development | BuildOptions.AutoRunPlayer; 

        // Setup for AltUnity
        AltUnityBuilder.AddAltUnityTesterInScritpingDefineSymbolsGroup (BuildTargetGroup.Android);
        AltUnityBuilder.InsertAltUnityInScene (buildPlayerOptions.scenes[0]);
        
        var results = BuildPipeline.BuildPlayer (buildPlayerOptions); //Building the game
        AltUnityBuilder.RemoveAltUnityTesterFromScriptingDefineSymbols (BuildTargetGroup.Android); //Removing AltunityTester

    } catch (Exception exception) {
        Debug.LogException (exception);
        // EditorApplication.Exit(1);
    }

}